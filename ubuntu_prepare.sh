#!/usr/bin/env bash

sed -i 's/# \(.*multiverse$\)/\1/g' /etc/apt/sources.list
apt-get update
apt-get -y upgrade
apt-get install -y build-essential git wget
apt-get install -y software-properties-common
apt-get install -y curl git htop man unzip vim wget

apt-get install -y postgresql postgresql-contrib postgresql-client php-cgi php-gd php-mcrypt php-pgsql php-zip
# apache2 libapache2-mod-php5 php5 php-cli php-cgi php-gd php-mcrypt php-pgsql php-zip
a2dismod mpm_event
a2enmod mpm_prefork

/etc/init.d/postgresql start
su - postgres
psql
ALTER USER postgres PASSWORD 'postgres';
\q

psql -h 127.0.0.1 -U postgres -d template1
update pg_database set encoding = 6, datcollate = 'en_US.UTF8', datctype = 'en_US.UTF8' where datname = 'template1';
create user bocauser with password 'boca' createdb;

vim /etc/locale.gen
locale-gen en_US.UTF-8
locale-gen

# wget -c 'https://www.ime.usp.br/~cassio/boca/download.php?filename=installv2.sh'

# vim private/conf.php

# php private/createdb.php

# # add boca.conf
# vim /etc/apache2/sites-enabled/000-default.conf

# /etc/init.d/apache2 start

# exit

# apt-get install quota debootstrap schroot
# cd /var/www/boca/tools/
# ./createbocajail.sh

# elinks http://172.22.0.2/src




