#!/usr/bin/env bash

service postgresql start

# # Comprobar si el archivo /etc/boca.conf existe
# if [ ! -f /etc/boca.conf ]; then
#     cp /root/boca/tools/boca.conf /etc/boca.conf
# else
#     echo "El archivo /etc/boca.conf ya existe."
# fi

if [ ! -d /var/www/boca ]; then
    make install
else
    echo "El boca ya esta instalado."
fi

# Comprueba si el usuario existe
if su - postgres -c "psql -tAc \"SELECT 1 FROM pg_roles WHERE rolname='$BOCA_DB_USER'\"" | grep -q 1; then
    echo "El usuario $BOCA_DB_USER ya existe."
else
    echo "Creando el usuario $BOCA_DB_USER..."
    su - postgres -c "psql -c \"CREATE USER $BOCA_DB_USER WITH PASSWORD '$BOCA_DB_PASSWORD';\""
    su - postgres -c "psql -c \"ALTER ROLE $BOCA_DB_USER SUPERUSER;\""
fi

# Comprueba si la base de datos existe
if su - postgres -c "psql -tAc \"SELECT 1 FROM pg_database WHERE datname='$BOCA_DB_NAME'\"" | grep -q 1; then
    echo "La base de datos $BOCA_DB_NAME ya existe."
else
    echo "Creando la base de datos $BOCA_DB_NAME..."
    su - postgres -c "psql -c \"CREATE DATABASE $BOCA_DB_NAME OWNER $BOCA_DB_USER;\""
	echo "bdcreated=y" >> /etc/boca.conf
fi


if grep -Fxq "bdserver=localhost" /etc/boca.conf; then
    echo "Ya esta configurada el dbhost."
else
    /usr/sbin/boca-config-dbhost localhost
fi


# boca-config-dbhost localhost

# Ejecuta el servidor Apache en primer plano
# apache2-foreground

# /usr/bin/supervisord
apache2ctl -D FOREGROUND