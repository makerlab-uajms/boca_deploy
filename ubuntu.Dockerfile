#FROM php:5.6-apache-stretch
#FROM php:7.3-apache-buster
# FROM php:8.1-apache-bullseye
FROM ubuntu:22.04


ENV DEBIAN_FRONTEND=noninteractive
ENV TERM=xterm-256color

MAINTAINER ubaldino<ozzirisc@gmail.com>

# WORKDIR /var/www

ENV LANG_WHICH en
ENV LANG_WHERE US
ENV ENCODING UTF-8
ENV LC_ALL C.${ENCODING}
ENV LANGUAGE ${LANG_WHICH}_${LANG_WHERE}.${ENCODING}
ENV LANG ${LANGUAGE}


ENV TZ America/La_Paz

# Apache settings
ENV APACHE_RUN_USER  www-data
ENV APACHE_RUN_GROUP www-data

# sed -i 's/# \(.*multiverse$\)/\1/g' /etc/apt/sources.list
RUN apt-get update
RUN apt-get -y upgrade
RUN apt-get install -y build-essential apt-utils git wget
RUN apt-get install -y apt-transport-https ca-certificates curl software-properties-common
RUN apt-get install -y curl git htop man unzip vim wget

RUN apt-get install -y apache2 postgresql postgresql-contrib postgresql-client

RUN apt-get update && apt-get install -y \
    libpng-dev \
    libzip-dev \
    libpq-dev \
    libcgi-pm-perl \
    libmcrypt-dev makepasswd sharutils libany-uri-escape-perl openssl locales tzdata  debconf

RUN apt-get install -y libxml2 libxml2-dev
RUN apt-get install -y debhelper-compat python3-matplotlib  \
        wget postgresql-client openssl libany-uri-escape-perl debconf makepasswd \
        sharutils debootstrap schroot quotatool makepasswd  sharutils wget \
        libany-uri-escape-perl openssl openssh-server
#logkeys
RUN apt-get install -y php-pgsql php-gd php-zip php-xml
RUN apt-get install -y libapache2-mod-php php-common php-bz2 php-curl php-gd \
        php-imagick php-intl php-json php-json-schema php-log php-mbstring \
        php-memcache php-memcached php-mime-type php-net-socket \
        php-net-whois php-parser php-readline php-redis php-soap php-tokenizer php-yaml \
        php-zip php-bcmath php-bz2 php-common php-curl php-gd php-xml php-zip \
        php-xmlrpc php-cli libphp-embed php-fpm php-cgi php-phpdbg php-mbstring

RUN apt -y install gcc make autoconf libc-dev pkg-config
RUN apt -y install libmcrypt-dev php-dev

# RUN curl -sSLf \
#         -o /usr/local/bin/install-php-extensions \
#         https://github.com/mlocati/docker-php-extension-installer/releases/latest/download/install-php-extensions && \
#     chmod +x /usr/local/bin/install-php-extensions && \
#     install-php-extensions mcrypt

RUN pecl install mcrypt
# php-mcrypt
# RUN docker-php-ext-install gd \
#     && docker-php-ext-install pgsql \
#     && docker-php-ext-install zip \
#     && docker-php-ext-install xml \
#     && docker-php-ext-enable mcrypt

RUN a2enmod cgi

COPY ./boca /root/boca

WORKDIR /root/boca

ENV BOCA_DB_USER=bocauser \
    BOCA_DB_PASSWORD=dAm0HAiC \
    BOCA_DB_NAME=bocadb


RUN echo "Setting time zone to '${TZ}'"
RUN ln -snf /usr/share/zoneinfo/$TZ /etc/localtime
RUN echo $TZ > /etc/timezone
RUN dpkg-reconfigure -f noninteractive tzdata

# RUN cp /var/www/boca/tools/000-boca.conf /etc/apache2/sites-available/000-default.conf


# RUN  /etc/init.d/postgresql start
# RUN su - postgres
# RUN psql
# RUN ALTER USER postgres PASSWORD 'postgres';
# RUN \q

# psql -h 127.0.0.1 -U postgres -d template1
# update pg_database set encoding = 6, datcollate = 'en_US.UTF8', datctype = 'en_US.UTF8' where datname = 'template1';
# create user bocauser with password 'boca' createdb;


# wget -c 'https://www.ime.usp.br/~cassio/boca/download.php?filename=installv2.sh'


# Add files.
# ADD ubuntu_prepare.sh /var/www/ubuntu_prepare.sh

# ADD ./boca /var/www/ubuntu_prepare.sh
# ADD ./boca /var/www/html


# 11
# ADD root/.gitconfig /root/.gitconfig
# ADD root/.scripts /root/.scripts

# Set environment variables.
#ENV HOME /root
RUN apt install -y supervisor

# CMD [ "/bin/bash" ]

#------------
#APACHE_CONFDIR=/etc/apache2
#APACHE_ENVVARS=$APACHE_CONFDIR/envvars

# docker build --rm --no-cache -t aloja .
# docker kill aloja
# docker rm aloja
# docker run --privileged --name aloja -v /sys/fs/cgroup:/sys/fs/cgroup:ro -d p3
# docker exec -it aloja bash
#------------
# pull


# Limpiamos la caché de APT para reducir el tamaño de la imagen
RUN apt-get clean && rm -rf /var/lib/apt/lists/*

# Iniciamos el servicio de Apache en segundo plano
# CMD ["apache2-foreground"]

# Las instrucciones previas de tu Dockerfile

# Copia el archivo init.sh al contenedor
COPY init.sh /usr/local/bin/

# Establece el comando por defecto para ejecutar el script init.sh
CMD ["/usr/local/bin/init.sh"]
